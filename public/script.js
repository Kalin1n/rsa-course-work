// VARIABLES
//var encrMSG = undefined;


// SUB FUNCTONS
function check(encrMSG){
  if ( encrMSG === undefined){
    return true;           // if message is empty
  }
  else {
    return false;          // if message alredy exist
  }
}

// SEND TO SERVER FUNCTONS
// Generate keys
function generateKeys(){
  if( check(window.encrMSG) === true){
    fetch ('http://localhost:5000/generateKeyPare',{
      headers : {
        'Accept' : 'application/json',
        'Content-type' : 'application/json'
      },
      method : 'POST'
    })
    .then( (res)=>{
      console.log(res);
      })
    .catch( (res) =>{
      console.log(res);
    })
  }
  else{
    alert("YOU ALREDY ENCRYPT. PLEASE RELOAD THE PAGE AND GENERATE A NEW KEY PARE");
  }
};

// Send textData
function sendText(){
  if( check(window.encrMSG) === true){
    fetch('http://localhost:5000/writeText',{
      headers : {
         'Accept' : 'application/json',
         'Content-Type' : 'application/json'
      },
      method : 'POST',
      body : JSON.stringify({ text : secret.value})
    })
    .then( (res) => {
      console.log(res);
    })
    .catch( (res) =>  {
      console.log(res);
    })
  }
  else{
    alert("YOU ALREDY ENCRYPT. PLEASE RELOAD THE PAGE AND GENERATE A NEW KEY PARE");
  }
}

// Encrypt
function encrypt(){
  if( check(window.encrMSG) === true){
    fetch('http://localhost:5000/encrypt',{
      headers : {
        'Accept' : 'application/json',
        'Content-Type' : 'application/json'
      },
      method : 'GET'
    })
    .then(function (res){
      return res.json()
    })
    .then(
      function (res){

        console.log(res.enc);
        var p = document.createElement('p')
        p.innerText = ` ENCRYPTED : ${res.enc}`; // TO HTML
        //encrMSG = res.enc; // TO VARIABLE
        document.body.appendChild(p);

      }
    )
    .catch((res)=>
      console.log(res)
    )
  }
  else{
    alert("YOU ALREDY ENCRYPT. PLEASE RELOAD THE PAGE AND GENERATE A NEW KEY PARE");
  }
}

function decrypt(){
  fetch('http://localhost:5000/decrypt',{
    headers :{
      'Accept' : 'application/json',
      'Content-Type': 'application/json'
    },
    method : 'GET',
  } )
  .then( function (res){
    return res.json()
      .then( function (res){
      console.log(res.dec);
      var p = document.createElement('p');
      p.innerText = `DECRYPTED : ${res.dec}`;
      document.body.appendChild(p);
  })})
  .catch((res) => {
    console.log(res)
  })

}
