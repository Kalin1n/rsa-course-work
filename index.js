// ALL MODULES AND COMPONENTS
const express = require('express');         // Express package
const rsa = require('node-rsa');            // RSA package
var bodyParser = require('body-parser')     // Body parser to JSON
var http = require('http');

// EXPRESS START
var app = express();  // EXPRESS START

const port = 5000;    // PORT
const keyBytes = 2048;  // LENGTH OF RSA KEY IN BYTES


app.use(express.static('public'));          // SEND FRONT END
app.use(bodyParser.json());                 // USE BODY PARSER

// ARRAYS FOR DATA
var rsaData = [];
var textData=[];
var secret;
// Keys Generation
app.post('/generateKeyPare', (req, res)=>{
    console.log(" POST REQUEST : KEY GENERATION");
    console.time('keyGen');
    var key = new rsa ({b:keyBytes});
    console.timeEnd('keyGen');
    rsaData.push(key);
    res.status(201).send("KEY GENERATED SUCSSFULLY");
})

app.get('/generateKeyPare', (req, res) => {
  console.log(" GET REQUEST : GIVE INFO ABOUT KEYS")
  var count = rsaData.length;
  console.log(` ${count} : KEYS EXIST`)
  res.send(` ${count} : KEYS EXIST`)

})

// SEND TEXT
app.post('/writeText', (req, res) =>{
  console.log("POST REQUEST : SEND TEXT ");
  app.use(bodyParser.json());
  textData.push(req.body);
  console.log("TEXT WRITED SUCSSFULLY")
  res.status(201);
})

app.get('/writeText', (req, res) => {
  console.log("GET REQUEST : GIVE INFO ABOUT TEXT");
  app.use(bodyParser.json());
  var count = textData.length;
  for ( var i = 0 ; i < textData.length ; i++){
    console.log(textData[i]);
  }
  console.log(` ${count} : TEXT ADDED`);
  res.send(
    `<h1>${textData}</h1>`);
})

// ENCRYPTION
app.get('/encrypt', (req, res) => {
  console.log('ENCRYPTION START!!!');
  console.log(textData[0].text);
  var encrypted = rsaData[0].encrypt(textData[0].text, 'base64');
  console.log("Encrypted message : ");
  console.log(encrypted);
  secret = encrypted;
  res.send({enc: encrypted});
})

// DECRYPTION
app.get('/decrypt', (req, res)=>{
  console.log('DECRYPTION START!!!');
  console.log(textData[0].text);
  var decrypted = rsaData[0].decrypt(secret, 'utf8');
  console.log('Decrypted messadge : ');
  console.log(decrypted);
  res.send({dec :decrypted});
})




// SERVER LISTEN
app.listen(port, () => {
  console.log(`Project listen on port : ${port}`);
});
